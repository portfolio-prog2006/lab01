module Lib
    ( helloWorld,
    Age,
    getAge,
    addNumber,
    addAge,
    mhead1,
    mhead2,
    mhead3,
    mhead4,
    mhead5
    ) where

-- function that prints "hello world" to standard output
helloWorld :: IO ()
helloWorld = putStrLn "hello world"

-- creating the Age variable
newtype Age = Age Int deriving (Show, Eq, Ord)


-- function that reads an Age variable from user
getAge :: IO Age
getAge = do
    input <- getLine    -- gets input from user
    let age = read input :: Int -- parses to an int using the read function
    return (Age age)    -- returns the int as an age variable


-- | function to add two numbers (of any numerical type) and returns the sum (Task 3)
--
-- >>> addNumber 4 5
-- 9
--
-- >>> addNumber 11 2
-- 13
addNumber :: Num a => a -> a -> a
addNumber x y = x + y

-- | function to adds two Age variables (of type Int) and returns the sum (Task 3)
--
-- >>> addAge (Age 4) (Age 5)
-- Age 9
--
-- >>> addAge (Age 11) (Age 2)
-- Age 13
addAge :: Age -> Age -> Age
addAge (Age x) (Age y) = Age (x + y)


----------------------
-- Functions to retrieve the first element of a list without using head function (Task 4) --
----------------------

-- | using pattern matching
--
-- >>> mhead1 [11,2,3,41,5]
-- 11
--
-- >>> mhead1 "hi"
-- 'h'
mhead1 :: [a] -> a
mhead1 (x:_) = x

-- | using case expression, with error handler
--
-- >>> mhead2 [11,2,3,41,5]
-- 11
--
-- >>> mhead2 "hi"
-- 'h'
mhead2 :: [a] -> a
mhead2 xs = case xs of
  (x:_) -> x
  []    -> error "Empty list"

-- | using list comprehension and the mhead1 function above with error handler
--
-- >>> mhead3 [11,2,3,41,5]
-- 11
--
-- >>> mhead3 "hello"
-- 'h'
mhead3 :: [a] -> a
mhead3 xs = if not (null xs) then mhead1 [x | x:_ <- [xs]] else error "Empty list"

-- | using foldr
--
-- >>> mhead4 [11,2,3,41,5]
-- 11
--
-- >>> mhead4 "hello"
-- 'h'
mhead4 :: [a] -> a
mhead4 xs = foldr (\x _ -> x) (error "Empty list") xs

-- | using maybe
--
-- >>> mhead5 [11,2,3,41,5]
-- Just 11
--
-- >>> mhead5 "hello"
-- Just 'h'
--
-- >>> mhead5 []
-- Nothing
mhead5 :: [a] -> Maybe a
mhead5 [] = Nothing
mhead5 (x:_) = Just x

