module Main (main) where

import Lib


main :: IO ()
main = do

  -- Task 1: Hello world
  putStrLn "\n\tTask 1: Hello World\n"
  helloWorld


-- Task 2: Hello Name
  putStrLn "\n\tTask 2: Hello Name\n"
  putStrLn "What's your name?"
  userInput <- getLine
  putStrLn $ "Hello " ++ userInput ++ " :)\n"


-- Task 3: Age
-- | starting with function declarations for addAge and addNumber
  putStrLn "\n\tTask 3: Age\n"
  putStrLn "Hi, what's your name?"
  userName <- getLine

{-
  -- First using addNum and no type safety
  putStrLn "And what is your age?"
  userAge <- getLine
-- Converting user input to an Int NB! No error handling in case input isn't a number
  let age1 = read userAge :: Int
-- using addNumber to add 10 to the user input
  let addedNum = addNumber age1 10
--result string
  putStrLn $ "Hello " ++ userName ++ ", in 10 years you'll be " ++ show addedNum ++ " years old :)\n"
-}

  -- Secondly, using addAge with type safety
  -- User input is directly converted, so won't need to do that as an extra step
  putStrLn "And what is your age?"
  userAge2 <- getAge
  putStrLn "How many years should be added to your age?"
  -- Getting number of years to add to the age
  addedToAge2 <- getAge
  -- using addAge to add 'addedToAge2' years to the user input
  let addedAge = addAge userAge2 addedToAge2
  --result string
  putStrLn $ "Hello " ++ userName ++ ", in " ++ show addedToAge2 ++  " years you'll be " ++ show addedAge ++ " years old :)\n"

-- Task 4: Get first element in a list using my mhead function
  let mList =  [1,2,3,4,5]
  putStrLn "\n\tTask 4: mhead functions\n"
  putStrLn "\nUsing pattern matching:"
  putStrLn $ "For the list: " ++ show mList ++ ", the first element is: " ++ show (mhead1 mList)

  putStrLn "\nUsing case expression:"
  putStrLn $ "For the list: " ++ show mList ++ ", the first element is: " ++ show (mhead2 mList)

  putStrLn "\nUsing list comprehension:"
  putStrLn $ "For the list: " ++ show mList ++ ", the first element is: " ++ show (mhead3 mList)

  putStrLn "\nUsing 'foldr':"
  putStrLn $ "For the list: " ++ show mList ++ ", the first element is: " ++ show (mhead4 mList)

  putStrLn "\nUsing 'Maybe':"
  putStrLn $ "For the list: " ++ show mList ++ ", the first element is: " ++ show (mhead5 mList)
